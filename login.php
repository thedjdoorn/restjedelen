<html>
<head>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Open+Sans|Overpass+Mono:600');
        html,body{
            height: 100%;
            width: 100%;
            display: flex;
            align-items:center;
            justify-content: center;
            background: url("dist/login-wp.jpg");
            background-size: cover;
        }

        hr {
            border: 0;
            height: 1px;
            background: #333;
            background-image: linear-gradient(to right, #ccc, #333, #ccc);
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: 5%;
            margin-right: 5%;
        }

        .container{
            border-radius: 25px;
            max-width: 70%;
            height: 75vh;
            flex: 1;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            padding: 1.5em;
            background-color: white;
            text-align: center;
        }

        .part{
            display: inline-block;
            flex: 1;
        }

        input{
            text-align: center;
            font-family: 'Open Sans', sans-serif;
            border-radius: 5px;
            border-width: 1px;
            border-color: black;
            border-style: solid;
            margin: 1em;
            padding: 0.5em;
        }

        input[type=number]{
            -webkit-appearance: textfield;
            -moz-appearance: textfield;
        }

        h4{
            font-family: 'Overpasss Mono', sans-serif;
            font-size: 2em;
            font-weight: 600;
        }

    </style>
</head>
<body>
<div class="container">
    <div id="login" class="part">
        <h4>Login</h4>
        <form method="post"  action="submitlogin.php">
            <input type="text" name="username" placeholder="Gebruikersnaam"><input type="password" name="password" placeholder="Wachtwoord"><input type="submit" value="Log in">
        </form>
    </div>
    <hr>
    <div id="register" class="part">
        <h4>Registreer</h4>
        <form method="post" action="submitregister.php">
        <input type="text" name="username" placeholder="Gebruikersnaam"><input type="password" name="password" placeholder="Wachtwoord"><input type="password" name="repeat" placeholder="Wachtwoord herhalen"><br>
        <input type="text" name="fname" placeholder="Voornaam"><input type="text" name="lname" placeholder="Achternaam"><br>
        <input type="email" name="email" placeholder="Emailadres"><br>
        <input type="text" name="street" placeholder="Straat"><input type="number" name="number" placeholder="Huisnummer"><input type="text" name="addition" placeholder="Toevoeging"><br><br>
        <input type="text" name="postal" placeholder="Postcode"> <input type="text" name="city" placeholder="Woonplaats"><br>
        <input type="submit" value="Registreer"></form>
    </div>
</div>
</body>
</html>
