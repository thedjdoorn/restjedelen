<html>
<head>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans">
</head>
<style>
    html, body {
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    input[type=text] {
        border: none;
        outline: none;
        font-size: 18pt;
        font-family: "Open Sans";
        width: 450px;
        background-color: transparent;
        margin-right: 45px;
        transition-duration: 0.5s;
        color: white;
    }

    input[type=text]:focus {
        border: none;
        border-bottom: solid white 2px;
    }

    input[type=submit] {
        font-size: 18pt;
        font-family: "Open Sans";
        background-color: transparent;
        border: solid white 2px;
        outline: 0 none;
        border-radius: 5px;
        color: white;
        padding-left: 5%;
        padding-right: 5%;
        box-shadow: none;
    }

    .large-title {
        color: #ffffff;
        font-family: "Lato";
    }

    #vid {
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translateX(-50%) translateY(-50%);
        transform: translateX(-50%) translateY(-50%);
        min-width: 100%;
        min-height: 100%;
        width: auto;
        height: auto;
        z-index: -1000;
        overflow: hidden;
    }

</style>
<body onload="getLocation()">


<video id="vid" video autobuffer loop autoplay>
    <source src="dist/1.mp4" type="video/mp4"/>
</video>

<h1 class="large-title" align="center" style="margin-top: 20%">Welkom op restjedelen!</h1>
<div align="center">
    <form action="results.php" align="center" method="post">
        <input id="city" type="text" placeholder="Woonplaats" name="city">
        <input type="submit" value="Zoek">
    </form>

<?php
  session_start();

   if(!isset($_SESSION['logged-in'])){ print('<a style="color: white; position: fixed; bottom: 15px; font-family: \'Open Sans\', sans-serif;" href="login.php">Log in</a>');}
   else {echo '<div style=" position: fixed; bottom: 15px; text-align: center" ><text style="color: white;font-family: \'Open Sans\', sans-serif;">Ingelogd als ' . $_SESSION['user'].' - </text><a style="color: white; font-family: \'Open Sans\', sans-serif;" href="newmeal.php">Verkoop restje</a></div>';}

?>
</div>
<script>
        var x = document.getElementById("city");

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            }
        }

        function showPosition(position) {
            var xmlhttp = new XMLHttpRequest();
            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&key=AIzaSyDxBoKTNxKeCAYHEgG7i7r_SYh0LTT41_c";
            console.log(url);
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    var json = JSON.parse(this.responseText);
                    printPosition(json);

                }
            };
            xmlhttp.open("GET", url, true);
            xmlhttp.send();

        }
        function printPosition(array) {
            var comp = array.results[0].address_components;
            var city;
            for(var i =0; i<comp.length; i++){
                if(comp[i].types[0] == "locality" && comp[i].types[1] == "political"){
                    city = comp[i].short_name;
                }
            }
            x.value = city;
        }
</script>
</body>
</html>
