<html>
<head>
    <link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/pure-min.css" xmlns="http://www.w3.org/1999/html">
    <script src="dist/moment.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Open+Sans|Overpass+Mono:600');

        html, body {
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            background: url("dist/results-wp.jpg");
            background-size: cover;
        }

        .container {
            border-radius: 25px;
            max-width: 70%;
            height: 75vh;
            flex: 1;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            padding: 1.5em;
            background-color: white;
            text-align: center;
        }
        .likeabutton {
            text-decoration: none; font: menu;
            display: inline-block; padding: 2px 8px;
            background: ButtonFace; color: ButtonText;
            border-style: solid; border-width: 2px;
            border-color: ButtonHighlight ButtonShadow ButtonShadow ButtonHighlight;
        }
        .likeabutton:active {
            border-color: ButtonShadow ButtonHighlight ButtonHighlight ButtonShadow;
        }
    </style>
</head>
<body>
<div class="container">
    <?php
    error_reporting(0);
    require_once 'dbinfo.php';
    $conn = new mysqli($host, $username, $password, $db);
    if ($query = mysqli_real_escape_string($conn, htmlentities($_POST['city']))) {
        echo("Resultaten voor " . $query . ":<br><br>");
    } else {
        echo("Geen stad ingevoerd\n");
    }

    if ($query != null) {
        if ($result = $conn->query("SELECT * FROM meals WHERE city ='" . $query . "' AND state=0;")){
            if (mysqli_num_rows($result) > 0) {
                echo ' <table class="pure-table">
        <thead>
            <tr>
                <td>
                    Naam
                </td>
                <td>
                    Beschrijving
                </td>
                <td>
                    Verkoper
                </td>
                <td>
                    Geplaatst
                </td>
                <td>
                    Prijs
                </td>
                <td>
                </td>
            </tr>
        </thead>';
                while ($row = mysqli_fetch_assoc($result)) {
                    echo '
            <tr>
                <td>
                '.$row["name"].'
                </td>
                <td>
                '.$row['description'].'
                </td>
                <td>
                '.$row['user'].'
                </td>
                <td class="moment" data-ts="'.$row['date'].'">
                </td>
                <td>
                '.$row['price'].'
                </td>
                <td>
                <a class="likeabutton" href="buy.php?id='.$row['mid'].'">Ga</a>
                </td>
            ';
                }
            } else {
                echo 'no meals availaboot';
            }
            $conn->close();
        } else {
            echo("Er is een fout opgetreden");
            echo $conn->errno;
        }
    } else {
        echo "Poar neemn";
    }

    ?>
</div>
<script>
    window.onload = function () {
        var items = document.getElementsByClassName("moment");
        for(var i = items.length - 1; i >= 0; i--){
            momenter(items[i]);
        }
    }

    momenter = function(obj) {
        moment.locale('nl');
        var time = moment.unix(obj.dataset.ts);
        obj.innerHTML = time.from(moment());
    }
</script>
</body>
</html>