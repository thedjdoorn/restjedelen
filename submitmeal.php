<?php
require_once 'dbinfo.php';
session_start();

if(isset($_SESSION['logged-in'])) {
    $link = new mysqli($host, $username, $password, $db);


    $getuser = "SELECT * FROM users, adresses INNER JOIN users ON users.address = adresses.aid WHERE uid = ".$_SESSION['user'].";";
    $userset = mysqli_query($link, $getuser);

    $userdata =  mysqli_fetch_assoc($userset);

    $city = $userdata['city'];
    $username = $userdata['uid'];
    $mealname = mysqli_real_escape_string($link, htmlentities($_POST['name']));
    $description = mysqli_real_escape_string($link, htmlentities($_POST['description']));
    $price = 1.085 * mysqli_real_escape_string($link, htmlentities($_POST['price']));

    $sql = "INSERT INTO meals (name, description, price, city, state, date, user) VALUES (".$mealname.", ".$description.",".$price.",".$city.",0,".time().",".$username.")";

    mysqli_query($link, $sql);
}