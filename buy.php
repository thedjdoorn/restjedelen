<?php
require_once 'dbinfo.php';
session_start();
if(isset($_SESSION['logged-in'])){
    $link = new mysqli('localhost', $username, $password, $db);

    $mid = mysqli_real_escape_string($link, htmlentities($_GET['id']));
    $query ="SELECT * FROM meals WHERE mid = ".$mid.";";

    $arr = mysqli_fetch_assoc(mysqli_query($link, $query));

    $update ="UPDATE meals SET state = 1 WHERE mid = ".$mid.";";
    $trans = "INSERT INTO transactions (seller, buyer, price, date) VALUES (".$arr['user'].", ".$_SESSION['user'].", ".$arr['price'].", ".time().")";

    mysqli_query($link, $update);
    mysqli_query($link, $trans);

    print 'Transaction successful';

} else {
    print 'Not Logged In!';
    header('refresh: 2; login.php');
}
?>