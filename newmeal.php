<html>
<head>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Open+Sans|Overpass+Mono:600');
        html,body{
            height: 100%;
            width: 100%;
            display: flex;
            align-items:center;
            justify-content: center;
            background: url("dist/login-wp.jpg");
            background-size: cover;
        }

        hr {
            border: 0;
            height: 1px;
            background: #333;
            background-image: linear-gradient(to right, #ccc, #333, #ccc);
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: 5%;
            margin-right: 5%;
        }

        .container{
            border-radius: 25px;
            max-width: 70%;
            height: 75vh;
            flex: 1;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            padding: 1.5em;
            background-color: white;
            text-align: center;
        }

        .part{
            display: inline-block;
            flex: 1;
        }

        input{
            text-align: center;
            font-family: 'Open Sans', sans-serif;
            border-radius: 5px;
            border-width: 1px;
            border-color: black;
            border-style: solid;
            margin: 1em;
            padding: 0.5em;
        }

        input[type=number]{
            -webkit-appearance: textfield;
            -moz-appearance: textfield;
        }

        h4{
            font-family: 'Overpasss Mono', sans-serif;
            font-size: 2em;
            font-weight: 600;
        }

    </style>
</head>
<?php
require_once 'dbinfo.php';
session_start();
if(isset($_SESSION['user'])){
    print'
    <body>
    <div class="container">
    <div id="new" class="part">
        <h4>Nieuwe maaltijd</h4>
        <form method="post"  action="submitmeal.php">
            <input type="text" name="name" placeholder="Naam"><br>
            <textarea rows="3" name="description" placeholder="Beschrijving"></textarea><br>
            <input type="number" name="price" placeholder="Prijs">
            <input type="submit" value="Voeg toe">
        </form>
    </div>

    ';
} else {
    header('Location: login.php');
}